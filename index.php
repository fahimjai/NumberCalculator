<?php

include_once 'vendor/autoload.php';

use Pondit\Calculator\NumberCalculator\Addition;
use Pondit\Calculator\NumberCalculator\subtraction;
use Pondit\Calculator\NumberCalculator\Multiplication;
use Pondit\Calculator\NumberCalculator\Division;

$addition1 = new Addition(1, 2);
$subtraction1 = new Subtraction(3, 4);
$multiplication1 = new Multiplication(5, 6);
$division1 = new Division(7, 8);

var_dump($addition1);

var_dump($subtraction1);

var_dump($multiplication1);

var_dump($division1);
